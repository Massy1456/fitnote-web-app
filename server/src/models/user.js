const mongoose = require('mongoose')
const validator = require('validator') // to check password strength and correct email
const Program = require('./program')

const userSchema = mongoose.Schema({
    name : { type: String, required: true },
    email : { type: String, required: true },
    password : { type: String, required: true },
    id : { type: String },
}, {
    timestamps: true,
})

// a virtual link to the Programs model
// this is so we can see each task pertaining to a specific user
userSchema.virtual('programs', {
    ref: 'Program',
    localField: '_id',
    foreignField: 'creator'
})

// virtual link to Post model
userSchema.virtual('posts', {
    ref: 'Post',
    localField: '_id',
    foreignField: 'creator'
})

const User = mongoose.model('User', userSchema)

module.exports = User