const mongoose = require('mongoose')

const postSchema = mongoose.Schema({
    title:{ type: String, required: true },
    textBody: { type: String, required: true },
    creator_name: { type: String, required: true},
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
}, {
    timestamps: true
})

const Post = mongoose.model('Post', postSchema)

module.exports = Post