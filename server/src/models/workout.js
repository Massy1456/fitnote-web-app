const mongoose = require('mongoose')

const workoutSchema = mongoose.Schema({
    workout_name:{
        type:String
    },
    exercises: [{
        type: mongoose.Types.ObjectId,
        ref: 'Exercise'
    }]
})

const Workout = mongoose.model('Workout', workoutSchema)

module.exports = Workout