const mongoose = require('mongoose')
// const validator = require('validator')

const programSchema = mongoose.Schema({

    program_name:{
        type: String,
    },
    creator: { // the req.userId of the user
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    workouts:[{
        type: mongoose.Types.ObjectId,
        ref: 'Workout'
    }]

})

const Program = mongoose.model('Program', programSchema)

module.exports = Program