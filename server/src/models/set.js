const mongoose = require('mongoose')

const setSchema = mongoose.Schema({
    weight:{
        type: String
    },
    repetitions:{
        type: String
    },
    rpe:{
        type: String
    }
})

const Set = mongoose.model('Set', setSchema)

module.exports = Set