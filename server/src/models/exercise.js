const mongoose = require('mongoose')

const exerciseSchema = mongoose.Schema({
    exercise_name:{
        type:String
    },
    notes:{
        type:String
    },
    exercise_subtitle:{
        type:String
    },
    sets:[{
        type: mongoose.Types.ObjectId,
        ref: 'Set'
    }]
})

const Exercise = mongoose.model('Exercise', exerciseSchema)

module.exports = Exercise