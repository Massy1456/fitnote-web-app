const jwt = require('jsonwebtoken')
const User = require('../models/user')

const secret = 'test';

const auth = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1]; // this grabs the token from the header
    const isCustomAuth = token.length < 500; // if the token length is less than 500, then its not a google auth token
    let decodedData;

    if (token && isCustomAuth) {   // if we have a token, and it is our custom token...    
        decodedData = jwt.verify(token, secret); // don't forget the secret!
        const user = await User.findOne({ _id: decodedData?.id }) // find the user with the decoded id
        if(!user) throw new Error()
        req.user = user // return userId and user
        req.userId = decodedData?.id;
    } else {
        decodedData = jwt.decode(token); 
        const user = await User.findOne({ _id: decodedData?.sub })
        if(!user) throw new Error()
        req.user = user // send back user
        req.userId = decodedData?.sub; // sub is googles name for specific id that differentiates users
    }    

    next();
  } catch (error) {
    console.log(error);
  }
};

module.exports = auth