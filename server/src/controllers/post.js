const Post = require('../models/post')

const createPost = async (req, res) => {
    const { title, textBody } = req.body
    try {
        const post = await new Post({
            title,
            textBody,
            creator_name: req.user.name,
            creator: req.user._id,
        })
        await post.save()
        res.status.send(post)
    } catch (e) {
        res.status(500).send(e)
    }
}

const getPosts = async (req, res) => {
    try {
        const posts = await Post.find({})
        res.status(200).send(posts)
    } catch (e) {
        res.status(500).send(e)
    }
}

module.exports = {
    createPost,
    getPosts
}
