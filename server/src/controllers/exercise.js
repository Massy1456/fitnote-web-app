const Exercise = require('../models/exercise')
const Workout = require('../models/workout')

const createExercise = async (req, res) => {
    
    const exerciseData = req.body

    try {
        const exercise = await new Exercise(exerciseData) //!

        await Workout.updateOne( //* await may not be necessary
            {_id: req.params.id}, // this is our parent's _id
            { $addToSet: { exercises: exercise._id}} // we attach our workout to our parent!
        )

        await exercise.save()
        res.status(201).send(exercise)
    } catch (e) {
        console.log(e)
    }
}

const getExercises = async (req, res) => {
    try {
        const exercises = await Exercise.find({})
                                    .populate({ path: 'sets'})

        res.status(200).send(exercises)
    } catch (e){
        console.log(e)
    }
}

const getExercise = async (req, res) => { //* come back to get
    try {
        const exercise = await Exercise.findById(req.params.id)
                                .populate({ path: 'sets'})

        if(!exercise){
            res.status(404).send()
        }
        res.status(200).send(exercise)
    } catch (e) {

    }
}

const deleteExercise = async (req, res) => {
    try {
        const exercise = await Exercise.findByIdAndDelete(req.params.id)
        res.status(200).send(exercise)
    } catch (e) {
        res.status(500).send(e)
    }
}

module.exports = {
    createExercise,
    getExercises,
    getExercise,
    deleteExercise
}