const Workout = require('../models/workout')
const Program = require('../models/program')

const createWorkout = async (req, res) => {
    
    const workoutData = req.body
    
    try {
        const workout = await new Workout(workoutData)

        await Program.updateOne( //* await may not be necessary
            {_id: req.params.id}, // this is our parent's _id
            { $addToSet: { workouts: workout._id}} // we attach our workout to our parent!
        )

        await workout.save() //* may not be necessary
        res.status(201).send(workout)

    } catch(e) {
        
    }
}

const getWorkouts = async (req, res) => {
    try {
        const workouts = await Workout.find({})
                                    .populate({
                                        path: 'exercises',
                                        populate: { 
                                            path: 'sets'
                                        }
                                    })
                                    
        res.status(200).send(workouts)
    } catch(e) {
        console.log(e)
    }
}

const getWorkout = async (req, res) => {
    try {
        const workout = await Workout.findById(req.params.id) //! fucking breakthrough!!
                                .populate({
                                    path: 'exercises',
                                    populate: { 
                                        path: 'sets'
                                    }
                                })

        if(!workout){
            res.status(404).send()
        }
        res.status(200).send(workout)
    } catch (e) {

    }
}

const deleteWorkout = async (req, res) => {
    try {
        const workout = await Workout.findByIdAndDelete(req.params.id)
        res.status(200).send(workout)
    } catch (e){
        res.status(500).send(e)
    }
}

module.exports = {
    createWorkout,
    getWorkouts,
    getWorkout,
    deleteWorkout
}