const Set = require('../models/set')
const Exercise = require('../models/exercise')

const createSet = async (req, res) => {
    
    const setData = req.body

    try {
        const newSet = new Set(setData)
        await Exercise.updateOne(
            {_id: req.params.id},
            { $addToSet: { sets: newSet._id}}
        )

        await newSet.save()
        res.status(201).send(newSet)
    } catch (e) {
        res.status(500).send(e)
    }
}

const getSets = async (req, res) => {
    try {
        const sets = await Set.find({})
        res.status(200).send(sets)
    } catch (e) {
        console.log(e)
    }
}

const getSet = async (req, res) => {
    try {
        const set = await Set.findById(req.params.id)
        if(!set){
            res.status(404).send()
        }
        res.status(200).send(set)
    } catch (e) {
        console.log(e)
    }
}

const deleteSet = async (req, res) => {
    try {
        const set = await Set.findByIdAndDelete(req.params.id)
        res.status(200).send(set)
    } catch (e) {
        res.status(500).send(e)
    }
}

module.exports = {
    createSet,
    getSets,
    getSet,
    deleteSet
}