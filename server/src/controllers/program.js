const Program = require('../models/program')

const createProgram = async (req, res) => {
    
    const programData = req.body
    
    try {
        const program = new Program({
            ...programData,
            creator: req.userId // coming from middleware
        }) // create a new program with the information requested
        await program.save() // save it to database
        res.status(201).send(program) // send it back to user
    } catch (e) {
        res.status(500).send(e)
    }
}



const getPrograms = async (req, res) => {
    
    try{
        // finds the users programs by their ID (info comes from auth middleware)
        const programs = await Program.find({ creator: req.user._id })
                                    .populate({
                                        path: 'workouts',
                                        populate: { 
                                            path: 'exercises',
                                            populate: {
                                                path: 'sets'
                                            } 
                                        }
                                    }).exec()

        res.status(200).send(programs)
    } catch (e) {
        res.status(500).send(e)
    }

}

const getProgram = async (req, res) => {
    try {
        const program = await Program.findById(req.params.id)
                                .populate({
                                    path: 'workouts',
                                    populate: { 
                                        path: 'exercises',
                                        populate: {
                                            path: 'sets'
                                        } 
                                    }
                                })

        if(!program){
            res.status(404).send()
        }
        res.status(200).send(program)
    } catch (e) {
        res.status(500).send(e)
    }
}

const updateProgram = async (req, res) => {
    const updates = Object.keys(req.body) // store keys of request as variable
    const program = await Program.findById(req.params.id) // find program by id
    if(!program){
        res.status(404).send()
    }
    try {
        updates.forEach((update) => { // for each update, update it in the new program
            program[update] = req.body[update]
        })
        await program.save()
        res.status(200).send(program)
    } catch (e) {
        res.status(500).send(e)
    }
}

const deleteProgram = async (req, res) => {
    try {
        const program = await Program.findByIdAndDelete(req.params.id)
        if(!program){
            res.status(404).send()
        }
        res.status(200).send()
    } catch (e) {
        res.status(500).send(e)
    }
}

module.exports = {
    createProgram,
    getPrograms,
    getProgram,
    updateProgram,
    deleteProgram
}