const express = require('express')
const { createPost, getPosts } = require('../controllers/post')
const auth = require('../middleware/auth')
const router = express.Router()

router.post('/post', auth, createPost)

router.get('/post', getPosts)

module.exports = router // link to index.js