const express = require('express')
const { createExercise, getExercises, getExercise, deleteExercise } = require('../controllers/exercise')
const auth = require('../middleware/auth')
const router = express.Router()

router.post('/exercise/:id', auth, createExercise) // use id of workout

router.get('/exercise', getExercises) // view all the created exercises

router.get('/exercise/:id', getExercise) // get one exercise by id

router.delete('/exercise/:id', auth, deleteExercise) // the id is of exercise

module.exports = router