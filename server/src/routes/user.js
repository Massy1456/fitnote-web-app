const express = require('express')
const { signin, signup } = require('../controllers/user')

const router = express.Router()

router.post('/user/signup', signup)

router.post('/user/signin', signin)

module.exports = router