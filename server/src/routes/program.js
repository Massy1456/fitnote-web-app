const express = require('express')
const { createProgram, getPrograms, getProgram, updateProgram, deleteProgram } = require('../controllers/program')
const auth = require('../middleware/auth')
const router = express.Router()

router.post('/program', auth, createProgram)

router.get('/program', auth, getPrograms)

router.get('/program/:id', getProgram)

router.patch('/program/:id', auth, updateProgram)

// router.patch('/program', addSet)

router.delete('/program/:id', auth, deleteProgram)

module.exports = router