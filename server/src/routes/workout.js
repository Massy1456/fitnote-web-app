const express = require('express')
const { createWorkout, getWorkouts, getWorkout, deleteWorkout } = require('../controllers/workout')
const auth = require('../middleware/auth')
const router = express.Router()

router.post('/workout/:id', auth, createWorkout) // id of program!

router.get('/workout', getWorkouts) // view all the workouts!

router.get('/workout/:id', getWorkout) // view all the workouts!

router.delete('/workout/:id', auth, deleteWorkout)

module.exports = router