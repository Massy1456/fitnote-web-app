const express = require('express')
const { createSet, getSets, getSet, deleteSet } = require('../controllers/set')
const router = express.Router()
const auth = require('../middleware/auth')

router.post('/set/:id', auth, createSet) // id of exercise

router.get('/set', getSets)

router.get('/set/:id', getSet)

router.delete('/set/:id', auth, deleteSet) 

module.exports = router