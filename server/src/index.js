const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

const userRoutes = require('./routes/user')
const programRoutes = require('./routes/program')
const workoutRoutes = require('./routes/workout')
const exerciseRoutes = require('./routes/exercise')
const setRoutes = require('./routes/set')
const postRoutes = require('./routes/post')

const app = express()
dotenv.config()

app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json({limit: "30mb", extended: false}))
app.use(bodyParser.urlencoded({limit: "30mb", extended: false}))
app.use(cors())

app.use(userRoutes)
app.use(programRoutes)
app.use(workoutRoutes)
app.use(exerciseRoutes)
app.use(setRoutes)
app.use(postRoutes)

app.get('/', (req, res) => {
    res.send('Hello to fitnote API')
})

const CONNECTION_URL = process.env.CONNECTION_URL
const PORT = process.env.PORT || 5000

// create connection with database
mongoose.connect(CONNECTION_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,}
    ).then(() => app.listen(PORT, () => console.log(`Server up on Port ${PORT}`))
    ).catch((error) => console.log(error.message))

